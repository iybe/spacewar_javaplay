package jogoPrincipal;

public class Global {

	public static final int ID_MENU_INICIAL = 0;
	public static final int ID_BATALHA = 1;
	public static final int ID_FINAL = 2;
	public static final int ID_RECORDES = 3;
	public static final int LARGURA = 800;
	public static final int ALTURA = 600;
}
