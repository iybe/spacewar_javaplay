package jogoPrincipal;

import java.awt.Graphics;

import javaPlay.GameObject;
import javaPlay.Sprite;

public class laser extends GameObject {

	private Sprite sprite;
	private double x;
	private double y;
	private double velY;
	private int dano;
	private Boolean presente;
	private Boolean amigo;
	
	public laser(double px, double py, double vy, int dano, Boolean am) {
		this.setX(px);
		this.setY(py);
		this.velY = vy;
		this.dano = dano;
		this.presente = true;
		this.setAmigo(am);
	}
	
	public void setSprite(Sprite sp) {
		this.sprite = sp;
	}
	
	public void step(long timeElapsed) {
		this.setY(this.getY() - this.velY);
	}

	public void draw(Graphics g) {
		if(getPresente()) {
			sprite.draw(g, (int)getX(), (int)getY());
		}
	}

	public Boolean getPresente() {
		return presente;
	}

	public void setPresente(Boolean presente) {
		this.presente = presente;
	}

	public int getDano() {
		return dano;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}
	
	public int wd() {
		return sprite.getAnimFrameWidth();
	}
	
	public int hei() {
		return sprite.getAnimFrameHeight();
	}

	public Boolean getAmigo() {
		return amigo;
	}

	public void setAmigo(Boolean amigo) {
		this.amigo = amigo;
	}

}
