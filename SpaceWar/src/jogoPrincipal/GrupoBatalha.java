package jogoPrincipal;

import java.awt.Graphics;
import java.util.ArrayList;

import javaPlay.GameObject;
import javaPlay.Sprite;

public class GrupoBatalha extends GameObject{

	private ArrayList<NaveInimiga> enemy;
	public static GrupoBatalha instance = null;
	private Boolean controle;
	private static Boolean ativo = false;
	
	public GrupoBatalha() {
		this.instance = null;
		setEnemy(new ArrayList<NaveInimiga>());
	}
	
	public static GrupoBatalha getInstance() {
		if(instance == null) {
			instance = new GrupoBatalha();
		}
		return instance;
	}
	
	public void step(long timeElapsed) {
		for(int i = 0; i < getEnemy().size(); i++) {
			NaveInimiga atual = this.getEnemy().get(i);
			atual.step(timeElapsed);
			atual.lancarLaser();
			if(i == getEnemy().size() - 1) {
				if(atual.getX() > 0) {
					this.controle = true;
				}
				if(this.controle && (atual.getX() + atual.getSprite().getAnimFrameWidth() < 0 || atual.getX() > Global.LARGURA)) {
					this.exluirGrupo();
				}
			}
		}
		
	}

	public void draw(Graphics g) {
		for(NaveInimiga n : getEnemy()) {
			n.draw(g);
		}
	}

	public void exluirGrupo() {
		this.getEnemy().clear();
		this.setAtivo(false);
	}
	
	public void criarGrupo(int qtd,double yi,double vx,double vy,int vida,int dist,int dl) {
		this.controle = false;
		this.setAtivo(true);
		Sprite img = null;
		try {
			img = new Sprite("imgs/nave2.png",1,32,32);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		for(int i = 0; i < qtd; i++) {
			NaveInimiga e = new NaveInimiga((double)(-((i+1)*img.getAnimFrameWidth() + (i*dist))),yi,vida,vx,vy,dl);
			e.setSprite(img);
			getEnemy().add(e);
		}
	}

	public ArrayList<NaveInimiga> getEnemy() {
		return enemy;
	}

	public void setEnemy(ArrayList<NaveInimiga> enemy) {
		this.enemy = enemy;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
