package io;


public class Recordista extends Jogador{
	private String pontuacao;
	private String dataRecorde;
	
	public Recordista() {
		super();
	}
	

	public Recordista( String pontuacao) {
		super();
		this.pontuacao = pontuacao;
	}


	public Recordista(String nome, String idade, String nick, String pontuacao, String dataRecorde) {
		super(nome, idade, nick);
		this.pontuacao = pontuacao;
		this.dataRecorde = dataRecorde;
	}

	public String getPontuacao() {
		return pontuacao;
	}

	public void setPontua��o(String pontuacao) {
		this.pontuacao = pontuacao;
	}

	public String getDataRecorde() {
		return dataRecorde;
	}

	public void setDataRecorde(String dataRecorde) {
		this.dataRecorde = dataRecorde;
	}
	
	public String getNick() {
		return super.getNick();
	}
	
	public String getIdade() {
		return super.getIdade();
	}
	
	public String getNome() {
		return super.getNome();
	}
	@Override
	public String toString() {
		return "Recordista [pontuacao=" + pontuacao + ", dataRecorde=" + dataRecorde + "]"+super.toString();
	}
	
	
	
}
