package io;

import java.io.IOException;
import java.util.HashMap;

public interface Escrivao {
	public HashMap leitora() throws IOException;
	public void escritora() throws IOException;
	
}
