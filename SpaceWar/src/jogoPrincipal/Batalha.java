package jogoPrincipal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import javaPlay.GameEngine;
import javaPlay.GameStateController;
import javaPlay.Keyboard;
import javaPlay.Sprite;

public class Batalha implements GameStateController {

	private Sprite fundo;
	private Nave mae;
	private int pontos = 0;
	
	public void load() {
		
	}

	public void unload() {
		
	}

	public void start() {
		try {
			//int qtd,double yi,double vx,double vy,int vida,int dist,int dl
			//GrupoBatalha.getInstance().criarGrupo(3,30,0.22,0.05,20,55,70);
			fundo = new Sprite("imgs/universo2.jpg",1,800,600);
			this.pontos = 0;
			
			mae = new Nave(400,300,50,0.2,0.1,60,0.1);
			Sprite danave = new Sprite("imgs/nave3.png",1,50,60);
			mae.setSprite(danave);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stop() {
		
	}

	public void step(long timeElapsed) 
	{
		if(mae.getLife() <= 0) {
			GrupoBatalha.getInstance().exluirGrupo();
			GrupoLaser.getInstance().limpar();
			GameEngine.getInstance().setNextGameStateController(Global.ID_FINAL);
			Final.getInstance().pontos = this.pontos;
		}
		
		Keyboard k = GameEngine.getInstance().getKeyboard();
		
        if(k.keyDown(Keyboard.UP_KEY) == true)
        {
            mae.moveUp();
        }
        if(k.keyDown(Keyboard.DOWN_KEY) == true)
        {
            mae.moveDown();
        }
        if(k.keyDown(Keyboard.LEFT_KEY) == true)
        {
            mae.moveLeft();
        }
        if(k.keyDown(Keyboard.RIGHT_KEY) == true)
        {
            mae.moveRight();
        }
        if(k.keyDown(Keyboard.SPACE_KEY) == true)
        {
        	mae.lancarLaser();
        }
        
        mae.step(timeElapsed);
        GrupoLaser.getInstance().step(timeElapsed);
        GrupoBatalha.getInstance().step(timeElapsed);
        this.colisaoCheck();
        
        if(GrupoBatalha.getInstance().getAtivo() == false) {
        	GrupoBatalha.getInstance().criarGrupo(3,30,0.22,0.05,20,55,70);
        }
        
	}

	public void draw(Graphics g) {
		
		fundo.draw(g, 0, 0);
		mae.draw(g);
		GrupoLaser.getInstance().draw(g);
		GrupoBatalha.getInstance().draw(g);
		
		g.setColor(Color.RED);
		g.setFont(new Font("serif",Font.BOLD,30));
		g.drawString("Pontuašao: "+this.pontos, 230, 60);
		g.drawString("Vida: "+mae.getLife(), 450, 60);
	}
	
	public void colisaoCheck() {
		for(laser l : GrupoLaser.getInstance().getLasers()) {
			if(mae.getLife() > 0 && l.getPresente() && l.getAmigo() == false && mae.collision((int)l.getX(), (int)l.getY(), l.wd(), l.hei())) {
				mae.perderVida(l.getDano());
				l.setPresente(false);
			}
			if(GrupoBatalha.getInstance().getAtivo()) {
				for(NaveInimiga n : GrupoBatalha.getInstance().getEnemy()) {
					if(n.getLife() > 0 && l.getPresente() && l.getAmigo() && n.collision((int)l.getX(), (int)l.getY(), l.wd(), l.hei())) {
						n.perderVida(l.getDano());
						l.setPresente(false);
						if(n.getLife() <= 0) {
							this.pontos += 10;
							//System.out.println(this.pontos);
						}
					}
					if(n.getLife() > 0 && mae.getLife() > 0 && n.collision((int)mae.getX(),(int)mae.getY(),mae.wd(),mae.hei()) == true) {
						mae.perderVida(mae.getLife());
						System.out.println((int)n.getX()+" "+(int)n.getY()+" "+n.wd()+" "+n.hei());
						System.out.println((int)mae.getX()+" "+(int)mae.getY()+" "+mae.wd()+" "+mae.hei());
					}
				}
			}
		}
		
	}

}
