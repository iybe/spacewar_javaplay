package jogoPrincipal;

import java.awt.Graphics;
import java.util.ArrayList;

import javaPlay.GameObject;

public class GrupoLaser extends GameObject {

	public static GrupoLaser instance = null;
	private ArrayList<laser> lasers;

	public GrupoLaser() {
		this.setLasers(new ArrayList<laser>());
	}

	public static GrupoLaser getInstance() {
		if (instance == null) {
			instance = new GrupoLaser();
		}
		return instance;
	}

	public void step(long timeElapsed) {
		for (int i = 0; i < getLasers().size() ; i++) {
			laser l = this.getLasers().get(i);
			l.step(timeElapsed);
			if(l.getY() > Global.ALTURA || l.getY() < 0) {
				getLasers().remove(i);
			}
		}
	}

	public void draw(Graphics g) {
		for (laser l : getLasers()) {
			l.draw(g);
		}
	}

	public void add(laser l) {
		getLasers().add(l);
	}

	public ArrayList<laser> getLasers() {
		return lasers;
	}

	public void setLasers(ArrayList<laser> lasers) {
		this.lasers = lasers;
	}
	
	public void limpar() {
		this.lasers.clear();
	}

}
