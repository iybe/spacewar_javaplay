package jogoPrincipal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Scanner;

import javaPlay.GameEngine;
import javaPlay.GameStateController;
import javaPlay.Keyboard;
import javaPlay.Mouse;
import javaPlay.Sprite;

public class MenuInicial implements GameStateController {

	private Sprite menu;

	public void load() {
		try {
			menu = new Sprite("imgs/menu1.jpg", 1, 800, 600);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void unload() {

	}

	public void start() {

	}

	public void step(long timeElapsed) {
		Mouse m = GameEngine.getInstance().getMouse();
		Keyboard k = GameEngine.getInstance().getKeyboard();
		
		if(k.keyDown(Keyboard.ESCAPE_KEY)) {
			GameEngine.getInstance().requestShutdown();
		}
		
		if (m.isLeftButtonPressed() == true) {
			Point p = m.getMousePos();

			if ((p.x >= 332) && (p.y >= 346) && (p.x <= 490) && (p.y <= 399)) {
				GameEngine.getInstance().setNextGameStateController(Global.ID_BATALHA);
			}else if ((p.x >= 300) && (p.y >= 445) && (p.x <= 523) && (p.y <= 490)) {
				GameEngine.getInstance().setNextGameStateController(Global.ID_RECORDES);
				//System.out.println("recordes");
			}else if ((p.x >= 376) && (p.y >= 539) && (p.x <= 447) && (p.y <= 568)) {
				GameEngine.getInstance().requestShutdown();
			} 
		
			//System.out.println(p.x+" "+p.y);
		}
	}

	public void draw(Graphics g) {
		menu.draw(g, 0, 0);
	}

	public void stop() {

	}

}
