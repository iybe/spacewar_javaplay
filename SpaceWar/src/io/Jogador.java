package io;

public abstract class Jogador {
	private String nome;
	private String idade;
	private String nick;
	
	
	
	
	
	public Jogador(String nome, String idade, String nick) {
		this.nome = nome;
		this.idade = idade;
		this.nick = nick;
	}
	
	public Jogador() {
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}

	@Override
	public String toString() {
		return "Jogador [nome=" + nome + ", idade=" + idade + ", nick=" + nick + "]";
	}
	
	
	
}
