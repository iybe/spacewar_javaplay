package jogoPrincipal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;

import io.GerenciadorRanking;
import io.Recordista;
import javaPlay.GameEngine;
import javaPlay.GameStateController;
import javaPlay.Keyboard;
import javaPlay.Mouse;
import javaPlay.Sprite;

public class Recordes implements GameStateController {

	private Sprite fundo;
	private static Recordes instance = null;
	private HashMap<Integer, Recordista > mapa;
	
	public static Recordes getInstance() {
		if(instance == null) {
			return instance = new Recordes();
		}
		return instance;
	}
	
	@Override
	public void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		try {
			fundo = new Sprite("imgs/universo2.jpg",1,800,600);
			mapa = GerenciadorRanking.getInstance().leitora(); 
			System.out.println("tam: "+mapa.size());
			for(int i = 0; i < mapa.size();i++) {
				System.out.println(GerenciadorRanking.getInstance().getInfo(i+1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void step(long timeElapsed) {
		// TODO Auto-generated method stub
		Mouse m = GameEngine.getInstance().getMouse();
		Keyboard k = GameEngine.getInstance().getKeyboard();
		
		if(k.keyDown(Keyboard.ENTER_KEY)) {
			GameEngine.getInstance().setNextGameStateController(Global.ID_MENU_INICIAL);
		}
		
		if (m.isLeftButtonPressed() == true) {
			Point p = m.getMousePos();

			if ((p.x >= 355) && (p.y >= 505) && (p.x <= 505) && (p.y <= 550)) {
				long atual = System.currentTimeMillis();
				//while(System.currentTimeMillis() - atual < 100) {}
				GameEngine.getInstance().setNextGameStateController(Global.ID_MENU_INICIAL);
			}
		
			//System.out.println(p.x+" "+p.y);
		}
	}

	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		fundo.draw(g, 0, 0);
		
		g.setColor(Color.RED);
		g.setFont(new Font("serif",Font.BOLD,50));
		g.drawString("Sair", 355, 550);
		
		for(int i = 0; i < mapa.size();i++) {
			//System.out.println(GerenciadorRanking.getInstance().getInfo(i+1));
			g.setFont(new Font("serif",Font.BOLD,30));
			g.drawString("Top "+(i+1)+" - "+GerenciadorRanking.getInstance().getInfo(i+1).getPontuacao()+" pontos", 300, 100 + (i*35));
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

}
