package jogoPrincipal;

import java.awt.Graphics;

import javaPlay.GameObject;
import javaPlay.Sprite;

public class Nave extends GameObject {

	private double x;
	private double y;
	private double velX;
	private double velY;
	private Sprite sprite;
	private int life;
	private laser ultimo;
	private int distTiro;
	private double excessoVelLaser;

	public Nave(double px, double py, int vida, double vx, double vy,int dt,double evl) {
		this.setX(px);
		this.setY(py);
		this.life = vida;
		this.velX = vx;
		this.velY = vy;
		this.ultimo = null;
		this.distTiro = dt;
		this.excessoVelLaser = evl;
	}

	public void setSprite(Sprite atual) {
		this.sprite = atual;
	}
	
	public Sprite getSprite() {
		return this.sprite;
	}

	public void step(long timeElapsed) {
		if(this.getX() + sprite.getAnimFrameWidth() > Global.LARGURA) {
			this.setX(Global.LARGURA - sprite.getAnimFrameWidth());
		}
		if(this.getX() < 0) {
			this.setX(0);
		}
		if(this.getY() + sprite.getAnimFrameHeight() > Global.ALTURA) {
			this.setY(Global.ALTURA - sprite.getAnimFrameHeight());
		}
		if(this.getY() < 0) {
			this.setY(0);
		}
	}

	public void draw(Graphics g) {
		if (this.life > 0) {
			sprite.draw(g, (int) this.getX(), (int) this.getY());
		}
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}
	
	public void perderVida(int vida) {
		this.life -= vida;
	}

	public void moveUp() {
		this.setY(this.getY() - this.velX);
	}

	public void moveDown() {
		this.setY(this.getY() + this.velX);
	}

	public void moveLeft() {
		this.setX(this.getX() - this.velY);
	}

	public void moveRight() {
		this.setX(this.getX() + this.velY);
	}
	
	public void lancarLaser() {
		if(this.life > 0 && (ultimo == null || (this.getY() - ultimo.getY() > this.distTiro))) {
			try {
				Sprite l = new Sprite("imgs/laser.png",1,6,13);
				
				laser arma = new laser((int)(this.getX()+((sprite.getAnimFrameWidth()+l.getAnimFrameWidth())/2)),this.getY(),this.velY + this.excessoVelLaser,10,true);
				arma.setSprite(l);
				GrupoLaser.getInstance().add(arma);
				
				ultimo = arma;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Boolean collision(int x2,int y2,int wd2,int hei2) {
		int x1 = (int) this.getX(),y1 = (int) this.getY(),wd1 = sprite.getAnimFrameWidth(),hei1 = sprite.getAnimFrameHeight();
		if(((x1 < x2) && (x2 < (wd1 + x1))) && ((y1 < y2) && (y2 < (y1 + hei1)))){
			//System.out.println(1);
			return true;
		}
		if(((x1 < x2 + wd2) && (x2 + wd2 < x1 + wd1)) && ((y1 < y2) && (y2 < y1 + hei1))) {
			//System.out.println(2);
			return true;
		}
		if( (x1 < x2) && (x2 < (x1 + wd1)) && (y1 < (y2 + hei2)) && (y2 + hei2 < (y1 + hei1))) {
			//System.out.println(3);
			return true;
		}
		if((x1 < x2 + wd2) && (x2 + wd2 < (x1 + wd1)) && (y1 < (y2 + hei2)) && (y2 + hei2 < y1 + hei1)) {
			//System.out.println(4);
			return true;
		}
		if((x2 < x1) && (x1 < (x2 + wd2)) && (y2 < y1) && (y1 < (y2 + hei2))) {
			//System.out.println(5);
			return true;
		}
		if((x2 < x1 + wd1) && (x1 + wd1 < x2 + wd2) && (y2 < y1) &&(y1 < y2 + hei2)) {
			//System.out.println(6);
			return true;
		}
		if((x2 < x1) && (x1 < x2 + wd2) && (y2 < y1 + hei1) && (y1 + hei1 < (y2 + hei2))) {
			//System.out.println(7);
			return true;
		}
		if((x2 < x1 + wd1) && (x1 + wd1 < x2 + wd2) && (y2 < y1 + hei1) && (y1 + hei1 < y2 + hei2)) {
			//System.out.println(8);
			return true;
		}
		return false;
	}

	public double getX() {
		return this.x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public int wd() {
		return sprite.getAnimFrameWidth();
	}

	public int hei() {
		return sprite.getAnimFrameHeight();
	}
	
}
