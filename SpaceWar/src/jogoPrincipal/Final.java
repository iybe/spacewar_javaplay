package jogoPrincipal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;

import io.GerenciadorRanking;
import io.Recordista;
import javaPlay.GameEngine;
import javaPlay.GameStateController;
import javaPlay.Keyboard;
import javaPlay.Mouse;
import javaPlay.Sprite;

public class Final implements GameStateController {

	private Sprite fundo;
	public static Final instance = null;
	public static int pontos;
	private int pos;
	
	public static Final getInstance() {
		if(instance == null) {
			return instance = new Final();
		}
		return instance;
	}
	
	@Override
	public void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		try {
			fundo = new Sprite("imgs/universo2.jpg",1,800,600);
			//String nome, String idade, String nick, String pontuacao, String dataRecorde
			if(GerenciadorRanking.getInstance().confereRecorde(pontos)) {
				this.pos = GerenciadorRanking.getInstance().insereRanking(new Recordista("--","--","--",Integer.toString(pontos),"--"));
				System.out.println(this.pos);
			}else {
				this.pos = -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void step(long timeElapsed) {
		// TODO Auto-generated method stub
		Mouse m = GameEngine.getInstance().getMouse();
		Keyboard k = GameEngine.getInstance().getKeyboard();
		
		if(k.keyDown(Keyboard.ENTER_KEY)) {
			GameEngine.getInstance().setNextGameStateController(Global.ID_MENU_INICIAL);
		}
		
		if (m.isLeftButtonPressed() == true) {
			Point p = m.getMousePos();

			if ((p.x >= 355) && (p.y >= 505) && (p.x <= 505) && (p.y <= 550)) {
				long atual = System.currentTimeMillis();
				//while(System.currentTimeMillis() - atual < 100) {}
				GameEngine.getInstance().setNextGameStateController(Global.ID_MENU_INICIAL);
			}
		
			//System.out.println(p.x+" "+p.y);
		}
	}

	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		fundo.draw(g, 0, 0);
		
		g.setColor(Color.RED);
		g.setFont(new Font("serif",Font.BOLD,80));
		g.drawString("Game Over", 210, 100);
		g.setFont(new Font("serif",Font.ITALIC,30));
		g.drawString("Sua Pontuašao: "+pontos, 210, 150);
		
		if(this.pos == -1) {
			g.drawString("Voce nao chegou no top 10 :("+pontos, 210, 250);
		}else {
			g.drawString("Top "+this.pos, 210, 250);
		}
		
		Font fonte = new Font("serif",Font.BOLD,50);
		g.setFont(fonte);
		g.drawString("Sair", 355, 550);
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

}
