package jogoPrincipal;

import javaPlay.GameEngine;

public class Main {

	public static void main(String[] args) {

		GameEngine.getInstance().addGameStateController(Global.ID_MENU_INICIAL, new MenuInicial());

		GameEngine.getInstance().addGameStateController(Global.ID_BATALHA, new Batalha());
		
		GameEngine.getInstance().addGameStateController(Global.ID_FINAL, new Final());
		
		GameEngine.getInstance().addGameStateController(Global.ID_RECORDES, new Recordes());

		GameEngine.getInstance().setStartingGameStateController(Global.ID_MENU_INICIAL);

		GameEngine.getInstance().run();

	}

}
