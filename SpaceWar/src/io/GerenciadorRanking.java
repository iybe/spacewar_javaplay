package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class GerenciadorRanking implements Escrivao {

	private static Map<Integer, Recordista> recordes = new HashMap<Integer, Recordista>();

	public static GerenciadorRanking e = null;

	public static GerenciadorRanking getInstance() {
		if (e == null) {
			return e = new GerenciadorRanking();
		}
		return e;
	}

	public HashMap leitora() throws IOException {// L� arquivo de recordes
		BufferedReader lerArq = new BufferedReader(new FileReader("recordes.txt"));

		String nome = "";
		String idade = "";
		String nick = "";
		String pontuacao = "";
		String data = "";

		Integer contador = 1;

		Recordista rec = new Recordista();

		while ((nome = lerArq.readLine()) != null) {

			idade = lerArq.readLine();
			nick = lerArq.readLine();
			pontuacao = lerArq.readLine();
			data = lerArq.readLine();
			rec = new Recordista(nome, idade, nick, pontuacao, data);
			this.recordes.put(contador, rec);
			contador++;
			if (contador > 10)
				break;
		}

		lerArq.close();

		return (HashMap) this.recordes;
	}

	public Recordista getInfo(Integer pos) {
		return this.recordes.get(pos);

	}

	public boolean confereRecorde(Integer pontuacao) {

		if (recordes.isEmpty() || recordes.size() < 10)
			return true;
		if (pontuacao > Integer.parseInt((recordes.get(recordes.size())).getPontuacao()))
			return true;
		return false;

	}

	public int insereRanking(Recordista novo) throws IOException {
		Recordista aux = new Recordista("-1");
		int pos = -1;

		recordes.put(recordes.size() + 1, aux);

		for (int i = 1; i <= recordes.size() && i < 11; i++) {
			Recordista temp = new Recordista();
			if (Integer.parseInt(recordes.get(i).getPontuacao()) < Integer.parseInt(novo.getPontuacao())
					&& i <= recordes.size()) {
				if (pos == -1)
					pos = i;
				temp = recordes.get(i);
				recordes.put(i, novo);
				novo = temp;
			}
		}

		if (recordes.size() > 10)
			recordes.remove(11);

		escritora();

		return pos;
	}

	public void escritora() throws IOException {

		PrintWriter pw = new PrintWriter("recordes.txt");
		pw.close();
		FileWriter fileWriter = new FileWriter("recordes.txt");
		PrintWriter printWriter = new PrintWriter(fileWriter);
		System.out.println("chegou: " + recordes.size());
		for (int i = 1; i <= recordes.size(); i++) {
			printWriter.println(recordes.get(i).getNome());
			printWriter.println(recordes.get(i).getIdade());
			printWriter.println(recordes.get(i).getNick());
			printWriter.println(recordes.get(i).getPontuacao());
			printWriter.println(recordes.get(i).getDataRecorde());
		}
		System.out.println("saiu: " + recordes.size());
		leitora();
		printWriter.close();

	}

}
